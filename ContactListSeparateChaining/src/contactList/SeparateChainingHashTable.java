package contactList;

import java.util.LinkedList;
import java.util.List;

public class SeparateChainingHashTable<AnyType>
{
	public SeparateChainingHashTable( )
	{
		this( DEFAULT_TABLE_SIZE );
	}


	public SeparateChainingHashTable( int size )
	{
		theLists = new LinkedList[ nextPrime( size ) ];
		for( int i = 0; i < theLists.length; i++ )
			theLists[ i ] = new LinkedList<Contact>( );
	}

	//The running time is theta(1) because it takes constant time to accesses and return a variable
	public int size() {
		return currentSize;
	}

	//The average running time is theta(1) because hashing takes constant and whichList never gets n element long in average case
	//because the load factor is 1, so although it takes theta(n) to rehash, whichList never gets n elements long by rehashing. So in average, contains and add does not have
	//traverse n elements.
	public void insertName( Contact x )
	{
		List<Contact> whichList = theLists[ myhash( x.getName() ) ];
		// System.out.println(myhash(x.getName()));


		if( !whichList.contains( x ) )
		{
			whichList.add( x );

			if( ++currentSize > theLists.length )
				rehashName( );
		}
	}

	//The average running time is theta(1) for the same reason as above
	public void insertNumber( Contact x )
	{
		List<Contact> whichList = theLists[ myhash( x.getNumber() ) ];
		//System.out.println(myhash(x.getNumber()));
		if( !whichList.contains( x ) )
		{
			whichList.add( x );

			if( ++currentSize > theLists.length )
				rehashNumber( );
		}
	}

	//The average running time is theta(1) because remove takes constant in average case for the same reason
	// as above
	public void removeName( Contact x )
	{
		List<Contact> whichList = theLists[ myhash( x.getName() ) ];
		if( whichList.contains( x ) )
		{
			whichList.remove( x );
			currentSize--;
		}
	}

	//The average running time is theta(1) for the same reason as above
	public void removeNumber( Contact x )
	{
		List<Contact> whichList = theLists[ myhash( x.getNumber() ) ];
		if( whichList.contains( x ) )
		{
			whichList.remove( x );
			currentSize--;
		}
	}


	//The average running time is theta(1) for the same reason as above
	public Contact getName( String x )
	{
		List<Contact> whichList = theLists[ myhash( x ) ];
		for (Contact item : whichList) {
			if (item.getName().equals(x)) {
				return item;
			}
		}
		return null;
	}

	//The average running time is theta(1) for the same reason as above
	public Contact getNumber( String x )
	{
		List<Contact> whichList = theLists[ myhash( x ) ];
		for (Contact item : whichList) {
			if (item.getNumber().equals(x)) {
				return item;
			}
		}
		return null;
	}

	//The running time is theta(1) hashing and contains take constant time
	public boolean containsName( Contact x )
	{
		List<Contact> whichList = theLists[ myhash( x.getName() ) ];
		return whichList.contains( x );
	}

	//The running time is theta(1) hashing and contains take constant time

	public boolean containsNumber( Contact x )
	{
		List<Contact> whichList = theLists[ myhash( x.getNumber() ) ];
		return whichList.contains( x );
	}

	//The running time is theta(1) because theLists length is constant
	public void makeEmpty( )
	{
		for( int i = 0; i < theLists.length; i++ )
			theLists[ i ].clear( );
		currentSize = 0;    
	}

	//The running time is theta(1) because key length is constant
	public static int hash( String key, int tableSize )
	{
		int hashVal = 0;

		for( int i = 0; i < key.length( ); i++ )
			hashVal = 37 * hashVal + key.charAt( i );

		hashVal %= tableSize;
		if( hashVal < 0 )
			hashVal += tableSize;

		return hashVal;
	}

	//The running time is theta(n) because it copies all n elements over to the new hashTable
	private void rehashName( )
	{
		List<Contact> [ ]  oldLists = theLists;

		// Create new double-sized, empty table
		theLists = new List[ nextPrime( 2 * theLists.length ) ];
		for( int j = 0; j < theLists.length; j++ )
			theLists[ j ] = new LinkedList<Contact>( );

		// Copy table over
		currentSize = 0;
		for( int i = 0; i < oldLists.length; i++ )
			for( Contact item : oldLists[ i ] )
				insertName( item );
	}

	//The runnning time is theta(n) for the same reason as above
	private void rehashNumber( )
	{
		List<Contact> [ ]  oldLists = theLists;

		// Create new double-sized, empty table
		theLists = new List[ nextPrime( 2 * theLists.length ) ];
		for( int j = 0; j < theLists.length; j++ )
			theLists[ j ] = new LinkedList<Contact>( );

		// Copy table over
		currentSize = 0;
		for( int i = 0; i < oldLists.length; i++ )
			for( Contact item : oldLists[ i ] )
				insertNumber( item );
	}

	//the running time is theta(1) because all assigning, comparing and arithmetic operations take constant time
	private int myhash( String key )
	{
		int hashVal = key.hashCode( );

		hashVal %= theLists.length;
		if( hashVal < 0 )
			hashVal += theLists.length;

		return hashVal;
	}

	private static final int DEFAULT_TABLE_SIZE = 101;
	private List<Contact> [ ] theLists; 
	private int currentSize;

	//the running time is theta(1) because all comparing and arithmetic operations take constant time
	private static int nextPrime( int n )
	{
		if( n % 2 == 0 )
			n++;

		for( ; !isPrime( n ); n += 2 )
			;

		return n;
	}

	//the running time is theta(1) because all comparing and arithmetic operations take constant time
	private static boolean isPrime( int n )
	{
		if( n == 2 || n == 3 )
			return true;

		if( n == 1 || n % 2 == 0 )
			return false;

		for( int i = 3; i * i <= n; i += 2 )
			if( n % i == 0 )
				return false;

		return true;
	}

	//The runninng time is theta(n) because it traverse all n elements and print every single one of them
	public void printAll() {
		for(List<Contact> list: theLists) {
			for(Contact x : list) {
				System.out.println(x.toString());
			}
		}
	}

}