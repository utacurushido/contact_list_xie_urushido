package contactList;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ContactList2Test {

	private ContactList2 contactListTest2;

	@Before
	public void setUp() throws Exception {
		contactListTest2 = new ContactList2();
		contactListTest2.insert("User 1", "123-456-7890");
		contactListTest2.insert("User 2", "123-456-7891");
	}

	@Test
	public void testInsert() {
		boolean insertOrNot = contactListTest2.insert("User 1", "123-456-7890");
		assertFalse(insertOrNot);
		insertOrNot = contactListTest2.insert("User 1", "123-456-7899");
		assertFalse(insertOrNot);
		insertOrNot = contactListTest2.insert("User 3", "123-456-7890");
		assertFalse(insertOrNot);
		insertOrNot = contactListTest2.insert("User 3", "123-456-7899");
		assertTrue(insertOrNot);
	}

	@Test
	public void testDeleteByNumber() {
		boolean deleteOrNot = contactListTest2.deleteByNumber("1234567892");
		assertFalse(deleteOrNot);
		deleteOrNot = contactListTest2.deleteByNumber("1234567890");
		assertTrue(deleteOrNot);
		String contactTest = contactListTest2.findByName("User 1");
		assertEquals(contactTest, "not found");
		contactTest = contactListTest2.findByNumber("123-456-7890");
		assertEquals(contactTest, "not found");	
	}

	@Test
	public void testDeleteByName() {
		boolean deleteOrNot = contactListTest2.deleteByName("User 3");
		assertFalse(deleteOrNot);
		deleteOrNot = contactListTest2.deleteByName("User 1");
		assertTrue(deleteOrNot);
		String contactTest = contactListTest2.findByName("User 1");
		assertEquals(contactTest, "not found");
		contactTest = contactListTest2.findByNumber("123-456-7890");
		assertEquals(contactTest, "not found");	
	}

	@Test
	public void testFindByName() {
		String contactTest = contactListTest2.findByName("User 1");
		assertNotNull(contactTest);
		assertEquals(contactTest, "User 1 1234567890");
		contactTest = contactListTest2.findByName("User X");
		assertEquals(contactTest, "not found");
	}

	@Test
	public void testFindByNumber() {
		String contactTest = contactListTest2.findByNumber("123-456-7890");
		assertNotNull(contactTest);
		assertEquals(contactTest, "User 1 1234567890");
		contactTest = contactListTest2.findByNumber("123-456-7899");
		assertEquals(contactTest, "not found");
	}


}
