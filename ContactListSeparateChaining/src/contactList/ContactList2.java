package contactList;


public class ContactList2 {

	SeparateChainingHashTable<Contact> contactListName;
	SeparateChainingHashTable<Contact> contactListNumber;

	public ContactList2() {
		contactListName = new SeparateChainingHashTable<Contact>();
		contactListNumber = new SeparateChainingHashTable<Contact>();
	}

	//The average running time is theta(1) because even though getNumber and insert take O(n) 
	//in the worst case when all n elements collide in one bucket, load factor of our insert method is 1 when rehash,
	//so hooked linked list is never n elements long in average, making the average case running time thata(1). hashing takes constant time
	public boolean insert(String name, String number) {
		number = removeCharacters(number);
		Contact toReturnNumber = contactListNumber.getNumber(number);
		Contact toReturnName = contactListName.getName(name);
		if (toReturnNumber == null && toReturnName == null) {
			if (isValid(number)) {
				Contact newContact = new Contact (name, number);
				contactListName.insertName(newContact);
				contactListNumber.insertNumber(newContact);
				return true;
			}
		}
		return false;
	}

	//The average running time is theta(1) because again, load factor is 1, so that 
	//  getNumber does not have to iterate through the hooked linked list that is n elements long in average case
	public boolean deleteByNumber(String number) {
		number = removeCharacters(number);
		Contact toReturn = contactListNumber.getNumber(number);
		if (toReturn == null) {
			return false;
		} else  {
			Contact toRemove = contactListNumber.getNumber(number);
			contactListNumber.removeNumber(toRemove);
			contactListName.removeName(toRemove);
			
			return true;
		}
	}
	
	//The average running time is theta(1) for the same reason as above.
	public boolean deleteByName(String name) {
		Contact toReturn = contactListName.getName(name);
		if (toReturn == null) {
			return false;
		} else  {
			Contact toRemove = contactListName.getName(name);
			contactListNumber.removeNumber(toRemove);
			contactListName.removeName(toRemove);
			
			return true;
		}
	}
	
	//The average running time is theta(1) for the same reason
	public String findByName(String name) {
		Contact toReturn = contactListName.getName(name);
		if (toReturn == null) {
			return "not found";
		} else {
			return contactListName.getName(name).toString();
		}
	}
	
	//The average running time is theta(1)
	public String findByNumber(String number) {
		number = removeCharacters(number);
		Contact toReturn = contactListNumber.getNumber(number);
		if (toReturn == null) {
			return "not found";
		} else {
			return contactListNumber.getNumber(number).toString();
		}
	}
	
	//the running time is theta(1) because it simply accesses to the currentSize
	//in SepareteChainingHashTable class
	public int size() {
		return contactListName.size();
	}
	
	//The running time is theta(n) where n is the number of elements in the hash table 
	//because it  traverses every single node of the hooked linked list and prints it 
	public void printAllContacts() {
		contactListName.printAll();
	}

	//The running time is theta(1) it takes constant operations to replace special cases and assign it
	public String removeCharacters(String number) {
		number = number.replaceAll("[^\\d]", "");
		return number;
	}

	//The running time is theta(1) it comparing and returning take constant time. 
	public boolean isValid(String number) {
		return number.length() == 10;
	}

	public static void main(String[] args) {
		ContactList2 pb = new ContactList2();
		pb.insert("Tom", "1234567890");
		pb.insert("Tim Lee", "123-456-7891");
		pb.insert("Tim Lee", "123-456-7891");
		System.out.println("printing all contacts");
		pb.printAllContacts();
		System.out.println("The number of contacts in this conntact list is: "+ pb.size());
		System.out.println();
		
		System.out.println("Searching for Tim Lee");
		System.out.println(pb.findByName("Tim Lee"));
		System.out.println();
		
		System.out.println("Searching for Tom");
		System.out.println(pb.findByName("Tom"));
		System.out.println();
		
		pb.deleteByName("Tim Lee");
		System.out.println("After deleting Tim Lee");
		pb.printAllContacts();
		System.out.println();
		
		System.out.println("Inserting Tim Lee back");
		pb.insert("Tim Lee", "123-456-7891");
		pb.printAllContacts();
		System.out.println();
		
		System.out.println("Searching for 123-456-7891");
		System.out.println(pb.findByNumber("123-456-7891"));
		System.out.println();
		
		System.out.println("The contact list is now");
		pb.printAllContacts();
		System.out.println();
		
		System.out.println("Searching for 1234567890");
		System.out.println(pb.findByNumber("1234567890"));
		System.out.println();
		
		System.out.println("deleting for 1234567890");
		pb.deleteByNumber("1234567890");
		pb.printAllContacts();
		System.out.println();
		
		System.out.println("success");
	}


}
