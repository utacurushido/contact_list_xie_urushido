package contactList;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SeparateChainingHashTableTest {

	private SeparateChainingHashTable<Contact> hTTest;
	private Contact contactTest1, contactTest2, contactTest3, contactTest4, contactTest5, contactTest6;


	@Before
	public void setUp() throws Exception {
		hTTest = new SeparateChainingHashTable<Contact>(5);
		contactTest1 = new Contact("User 1", "1234567891");
		contactTest2 = new Contact("User 2", "1234567892");
		contactTest3 = new Contact("User 3", "1234567893");
		contactTest4 = new Contact("User 4", "1234567894");
		contactTest5 = new Contact("User 5", "1234567895");
		contactTest6 = new Contact("User 6", "1234567896");

	}

	@Test
	public void testInsertName() {
		hTTest.insertName(contactTest1);//hashCode 0
		assertEquals(hTTest.getName("User 1"), contactTest1); 
		hTTest.insertName(contactTest2);//hashCode 1
		assertEquals(hTTest.getName("User 2"), contactTest2); 
		hTTest.insertName(contactTest1);
		assertEquals(hTTest.size(), 2);
		hTTest.insertName(contactTest4); //3
		hTTest.insertName(contactTest5); //4
		hTTest.insertName(contactTest6); //0 collision should occur w contactTest1
		assertNotEquals(hTTest.getName("User 1"), contactTest6);
		assertEquals(hTTest.getName("User 6"), contactTest6);
		System.out.println();
		hTTest.insertName(contactTest3); //2 hT should rehash(resize)
		assertEquals(hTTest.getName("User 3"), contactTest3);
	}

	@Test
	public void testInsertNumber() {
		hTTest.insertNumber(contactTest1);
		assertEquals(hTTest.getNumber("1234567891"), contactTest1); //hashCode 2
		hTTest.insertNumber(contactTest2);
		assertEquals(hTTest.getNumber("1234567892"), contactTest2); //hashCode 3
		hTTest.insertNumber(contactTest1);
		assertEquals(hTTest.size(), 2);
		hTTest.insertNumber(contactTest4); //0
		hTTest.insertNumber(contactTest5); //1
		hTTest.insertNumber(contactTest6); //2 collision should occur w contactTest1
		assertNotEquals(hTTest.getNumber("1234567891"), contactTest6);
		assertEquals(hTTest.getNumber("1234567896"), contactTest6);
		System.out.println();
		hTTest.insertNumber(contactTest3); //2 hT should rehash(resize)
		assertEquals(hTTest.getNumber("1234567893"), contactTest3);

	}

	@Test
	public void testRemoveName() {
		hTTest.insertName(contactTest1); //hashCode 0
		hTTest.insertName(contactTest2);//hashCode 1
		hTTest.removeName(contactTest1);
		assertNull(hTTest.getName("User 1"));
		hTTest.removeName(contactTest1);
		assertEquals(hTTest.size(), 1);
	}

	@Test
	public void testRemoveNumber() {
		hTTest.insertNumber(contactTest1); //hashCode 0
		hTTest.insertNumber(contactTest2);//hashCode 1
		hTTest.removeNumber(contactTest1);
		assertNull(hTTest.getNumber("1234567891"));
		hTTest.removeNumber(contactTest1);
		assertEquals(hTTest.size(), 1);
	}

	@Test
	public void testContainsName() {
		hTTest.insertName(contactTest1);//hashCode 0
		assertTrue(hTTest.containsName(contactTest1));
		hTTest.insertName(contactTest6); //0 collision, added to linked list
		assertTrue(hTTest.containsName(contactTest6));
		assertFalse(hTTest.containsName(contactTest5));
	}

	@Test
	public void testContainsNumber() {
		hTTest.insertNumber(contactTest1);//hashCode 2
		assertTrue(hTTest.containsNumber(contactTest1));
		hTTest.insertNumber(contactTest6); //2 collision, added to linked list
		assertTrue(hTTest.containsNumber(contactTest6));
		assertFalse(hTTest.containsNumber(contactTest5));
	}

	@Test
	public void testGetName() {
		hTTest.insertName(contactTest1);
		hTTest.insertName(contactTest6);
		assertEquals(hTTest.getName("User 1"), contactTest1);
		assertEquals(hTTest.getName("User 6"), contactTest6);

	}

	@Test
	public void testGetNumber() {
		hTTest.insertNumber(contactTest1);
		hTTest.insertNumber(contactTest6);
		assertEquals(hTTest.getNumber("1234567891"), contactTest1);
		assertEquals(hTTest.getNumber("1234567896"), contactTest6);

	}



}
