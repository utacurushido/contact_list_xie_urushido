package contactList;

public class Contact {
	private String name, number;

	// public Contact (String firstName, String lastName, String number) {
	// this.name = firstName + " " + lastName;
	// this.number = number;
	// }

	public Contact(String name, String number) {
		this.name = name;
		this.number = number;
	}

	public String getName() {
		return this.name;
	}

	public String getNumber() {
		return this.number;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String toString() {
		return name + " " + number;
	}
}
