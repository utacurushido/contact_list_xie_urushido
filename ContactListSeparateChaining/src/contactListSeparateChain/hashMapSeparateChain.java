// package contactListSeparateChain;
//
// import java.util.LinkedList;
// import java.util.List;
//
//
// import java.util.ArrayList;
//
// class hashNode<K, V>{
// K myKey;
// V myValue;
// hashNode<K, V> next;
//
// public hashNode(K key, V value) {
// myKey= key;
// myValue = value;
// }
// }//HashNode
//
// public class hashMapSeparateChain<K, V> {
//
// private static final int DEFAULT_TABLE_SIZE = 101;
//
// /** The array of Lists. */
// private int currentSize;
// private hashNode<K, V>[] theArrayOfNodes;
//
//
// /**
// * Construct the hash table.
// */
// public hashMapSeparateChain( )
// {
// this( DEFAULT_TABLE_SIZE );
// }
//
// /**
// * Construct the hash table.
// * @param size approximate table size.
// */
// public hashMapSeparateChain( int size )
// {
// theArrayOfNodes = new hashNode[ nextPrime( size ) ];
// // for( int i = 0; i < theArrayOfNodes.length; i++ )
// // theArrayOfNodes[ i ] = null;
// }
//
// /**
// * Insert into the hash table. If the item is
// * already present, then do nothing.
// * @param x the item to insert.
// */
// public void put(K key, V value ){
// if( this.containsKey( key ) ){
// return ;
// }
// int index = myhash( key );
// hashNode<K, V> head = theArrayOfNodes[index];
// currentSize++;
// hashNode<K, V> newNode = new hashNode<K, V>(key, value);
// newNode.next = head;
//
// // Rehash; see Section 5.5
// if( ++currentSize > theArrayOfNodes.length )
// rehash( );
//
// }
//
// /**
// * Remove from the hash table.
// * @param x the item to remove.
// */
// public void remove( K key ){
// int index = myhash( key );
// hashNode<K, V> head = theArrayOfNodes[index];
// hashNode<K ,V> prev = null;
// while (head !=null) {
// if (head.myKey.equals(key)) {
// break;
// }//key is found
// prev =head;
// head =head.next;
// }//while
//
// if (head == null)
// return ;
//
// currentSize--;
//
// if(prev!= null) {
// prev.next = head.next;
// }else {
// theArrayOfNodes[index] = head;
// }
// }//remove
//
// /**
// * Find an item in the hash table.
// * @param x the item to search for.
// * @return true if x is not found.
// */
// public boolean containsKey(K key )
// {
// int index = myhash( key );
// hashNode<K, V> head = theArrayOfNodes[index];
//
// while(head != null) {
// if(head.myKey.equals(key)) {
// return true;
// }//if
// head = head.next;
// }
// return false;
// }
//
// public V get(K key) {
// int index = myhash( key );
// hashNode<K, V> head = theArrayOfNodes[index];
//
// while(head != null) {
// if(head.myKey.equals(key)) {
// return head.myValue;
// }//if
// head = head.next;
// }
// return null;
// }
//
// /**
// * Make the hash table logically empty.
// */
// public void makeEmpty( )
// {
// for( int i = 0; i < theArrayOfNodes.length; i++ )
// theArrayOfNodes[ i ].clear( );
// currentSize = 0;
// }
//
// /**
// * A hash routine for String objects.
// * @param key the String to hash.
// * @param tableSize the size of the hash table.
// * @return the hash value.
// */
// public static int hash( String key, int tableSize )
// {
// int hashVal = 0;
//
// for( int i = 0; i < key.length( ); i++ )
// hashVal = 37 * hashVal + key.charAt( i );
//
// hashVal %= tableSize;
// if( hashVal < 0 )
// hashVal += tableSize;
//
// return hashVal;
// }
//
// private void rehash( )
// {
// hashNode<K,V>[ ] oldLists = theArrayOfNodes;
//
// // Create new double-sized, empty table
// theArrayOfNodes = new List[ nextPrime( 2 * theArrayOfNodes.length ) ];
// for( int j = 0; j < theArrayOfNodes.length; j++ )
// theArrayOfNodes[ j ] = new LinkedList<>( );
//
// // Copy table over
// currentSize = 0;
// for( List<AnyType> list : oldLists )
// for( AnyType item : list )
// insert( item );
// }
//
// private int myhash(K x )
// {
// int hashVal = x.hashCode( );
//
// hashVal %= theArrayOfNodes.length;
// if( hashVal < 0 )
// hashVal += theArrayOfNodes.length;
//
// return hashVal;
// }
//
//
//
// /**
// * Internal method to find a prime number at least as large as n.
// * @param n the starting number (must be positive).
// * @return a prime number larger than or equal to n.
// */
// private static int nextPrime( int n )
// {
// if( n % 2 == 0 )
// n++;
//
// for( ; !isPrime( n ); n += 2 )
// ;
//
// return n;
// }
//
// /**
// * Internal method to test if a number is prime.
// * Not an efficient algorithm.
// * @param n the number to test.
// * @return the result of the test.
// */
// private static boolean isPrime( int n )
// {
// if( n == 2 || n == 3 )
// return true;
//
// if( n == 1 || n % 2 == 0 )
// return false;
//
// for( int i = 3; i * i <= n; i += 2 )
// if( n % i == 0 )
// return false;
//
// return true;
// }
// }
