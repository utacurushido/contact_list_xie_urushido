package contactListSeparateChain;


import java.util.Scanner;



public class ContactList {

	 Scanner input;
	MyHashMapSeparateChain<String, Contact> contactListName;
	MyHashMapSeparateChain<String, Contact> contactListNumber;

	public ContactList() {
		 input = new Scanner(System.in);
		contactListName = new MyHashMapSeparateChain<String, Contact>();
		contactListNumber = new MyHashMapSeparateChain<String, Contact>();
	}

	public boolean insert(String name, String number) {
		number = removeCharacters(number);
		if (!containsName(name) && !containsNumber(number)) {
			if (isValid(number)) {
				Contact newContact = new Contact(name, number);
				contactListName.add(name, newContact);
				contactListNumber.add(number, newContact);
				return true;
			}
		}
		return false;
	}

	public boolean deleteByNumber(String number) {
		if (!containsNumber(number)) {
			return false;
		} else {
			contactListName.remove(contactListNumber.get(number).getName());
			contactListNumber.remove(number);
			return true;
		}
	}

	public boolean deleteByName(String name) {
		if (!containsName(name)) {
			return false;
		} else {
			contactListNumber.remove(contactListName.get(name).getNumber());
			contactListName.remove(name);
			return true;
		}
	}

	public boolean containsName(String name) {
		return contactListName.get(name) != null;
	}

	public boolean containsNumber(String number) {
		number = removeCharacters(number);
		return contactListNumber.get(number) != null;

	}

	public Contact findByName(String name) {
		if (!containsName(name)) {
			return null;
		} else {
			return contactListName.get(name);
		}
	}

	public Contact findByNumber(String number) {
		number = removeCharacters(number);
		if (!containsNumber(number)) {
			return null;
		} else {
			return contactListNumber.get(number);
		}
	}

	// public String next(){
	// return input.next();
	// }

	public void printAllContacts() {
		contactListNumber.printAll();
	}

	public String removeCharacters(String number) {
		number = number.replaceAll("[^\\d]", "");
		return number;
	}

	public boolean isValid(String number) {
		return number.length() == 10;
	}

	public static void main(String[] args) {
//		ContactList pb = new ContactList();
//		pb.insert("Tom", "123-456-7890");
//		pb.insert("Tim Lee", "1234566789");
//		System.out.println("printing all contacts");
//		pb.printAllContacts();
//
//		System.out.println("Searching for Tim Lee");
//		System.out.println(pb.findByName("Tim Lee"));
//
//		System.out.println("After deleting Tim Lee, the contact is");
//		if (pb.deleteByName("Tim Lee")) {
//			System.out.println("Tim Lee is deleted");
//		} else {
//			System.out.println("deletionn failed");
//		}
//		pb.printAllContacts();
//
//		System.out.println("Searching for 1234566789");
//		System.out.println(pb.findByNumber("1234566789"));
//
//		pb.insert("Tim Lee", "1234566789");
//		System.out.println("printing all contacts");
//		pb.printAllContacts();
//		System.out.println("Searching for 1234566789");
//		System.out.println(pb.findByNumber("1234566789"));
//		System.out.println("Searching for Tim Lee");
//		System.out.println(pb.findByName("Tim Lee"));
//
//		pb.deleteByNumber("1234567890");
//		pb.printAllContacts();
//		
		ContactList pb = new ContactList();
		pb.insert("Tom", "1234567890");
		pb.insert("Tim Lee", "(123)456-6789");
		//		System.out.println("printing all contacts");
		//		pb.printAllContacts();
		//		System.out.println("Searching for Tim Lee");
		//		System.out.println(pb.findByName("Tim Lee"));
		//		pb.deleteByName("Tim Lee");
		//		pb.printAllContacts();
		//		
		//		System.out.println("Searching for 1234566789");
		//		System.out.println(pb.findByNumber("1234566789"));
		//		
		//		pb.insert("Tim Lee", "1234566789");
		//		System.out.println("printing all contacts");
		//		pb.printAllContacts();
		//		System.out.println("Searching for 1234566789");
		//		System.out.println(pb.findByNumber("1234566789"));
		//		System.out.println("Searching for Tim Lee");
		//		System.out.println(pb.findByName("Tim Lee"));
		//		
		//		pb.deleteByNumber("1234567890");
		//		pb.printAllContacts();
		//	
		boolean exit = false;
		while(!exit) {
			System.out.println("\nPlease enter the prompt number of what you would like to do: ");
			System.out.println("1:Insert a contact to this contact list \n2:Delte a contact by name \n3:Delete a contact by number\n4:Look up a contact by name\n5:Look up a contact by number\n6:Print all the contacts on this contact list\n7:Exist" );
			String promptNumber;
			promptNumber = pb.input.next();
			switch (promptNumber) {
			case "1":
				pb.input.nextLine();
				System.out.println("What is the contact name?");
				String name =pb.input.nextLine();
				System.out.println("What is the contact number?");
				String number = pb.input.nextLine();
				if(pb.insert(name, number)) {
					System.out.println("Contact added");
				}else {
					System.out.println("Failed to add. We can't accept the name or the number that is already on the list.");
				}
				break;
			case "2":
				pb.input.nextLine();
				System.out.println("What is the name of the contact you would like to delete?");
				name = pb.input.nextLine();
				if(pb.deleteByName(name)) {
					System.out.println("Contact deleted");
				}else {
					System.out.println("This name was not found. Could not delete.");
				}
				break;
			case "3":
				pb.input.nextLine();
				System.out.println("What is the number of the contact you would like to delete?");
				number = pb.input.nextLine();
				if(pb.deleteByName(number)) {
					System.out.println("Contact deleted");
				}else {
					System.out.println("This number was not found. Could not delete.");
				}
				break;
			case "4":
				pb.input.nextLine();
				System.out.println("What is the name of the contact you would like to find?");
				String nameToFind = pb.input.nextLine();
				if(pb.findByName(nameToFind)!=null){
					System.out.println(pb.findByName(nameToFind).toString());
				}else {
					System.out.println("No contact with that name was found");
				}
				break;
			case "5":
				pb.input.nextLine();
				System.out.println("What is the number of the contact you would like to find?");
				String numberToFind = pb.input.nextLine();
				if(pb.findByNumber(numberToFind)!=null){
					System.out.println(pb.findByNumber(numberToFind).toString());
				}else {
					System.out.println("No contact with that number was found");
				}
				break;
			case "6":
				pb.printAllContacts();
				break;
			case "7":
				exit = true;
				break;
			default:
				System.out.println("Invalid input. Please put 1 through 6 ");
				break;
			}


		}
	}

}
