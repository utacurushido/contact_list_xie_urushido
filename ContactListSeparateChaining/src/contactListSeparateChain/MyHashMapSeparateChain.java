package contactListSeparateChain;

import java.util.ArrayList;

public class MyHashMapSeparateChain<K, V> {
	ArrayList<HashNode<K, V>> bucket = new ArrayList<>();
	int size = 10;
	int count;

	public MyHashMapSeparateChain() {
		for (int i = 0; i < size; i++) {
			bucket.add(null);
		}
	}

	public int getCount() {
		return count;
	}

	public boolean isEmpty() {
		return count == 0;
	}

	private int getBucketIndex(K key) {
		int hashCode = key.hashCode();
		hashCode %= size;
		if (hashCode < 0) {
			hashCode += size;
		}
		return hashCode;
	}

	public V get(K key) {
		int index = getBucketIndex(key);
		HashNode<K, V> node = bucket.get(index);
		while (node != null) {
			if (node.key.equals(key)) {
				return node.value;
			}
			node = node.next;
		}
		return null;
	}

	public V remove(K key) {
		int index = getBucketIndex(key);
		HashNode<K, V> node = bucket.get(index);
		if (node == null) {
			return null;
		}
		if (node.key.equals(key)) {
			V val = node.value;
			node = node.next;
			bucket.set(index, node);
			count--;
			return val;
		} else {
			HashNode<K, V> prev = null;
			while (node != null) {
				if (node.key.equals(key)) {
					prev.next = node.next;
					count--;
					return node.value;
				}
				prev = node;
				node = node.next;
			}

		}
		count--;
		return null;
	}

	public void add(K key, V value) {
		int index = getBucketIndex(key);
		// System.out.println(index);
		HashNode<K, V> node = bucket.get(index);
		HashNode<K, V> toAdd = new HashNode<>();
		toAdd.key = key;
		toAdd.value = value;
		if (node == null) {
			bucket.set(index, toAdd);
			count++;
		} else {
			while (node != null) {
				if (node.key.equals(key)) {
					node.value = value;
					count++;
					break;
				}
				node = node.next;
			}
			if (node == null) {
				node = bucket.get(index);
				toAdd.next = node;
				bucket.set(index, toAdd);
				count++;
			}
		}
		if ((1.0 * count) / size > 0.7) {
			ArrayList<HashNode<K, V>> temp = bucket;
			bucket = new ArrayList<>();
			size = 2 * size;
			for (int i = 0; i < size; i++) {
				bucket.add(null);
			}
			for (HashNode<K, V> n : temp) {
				while (n != null) {
					add(n.key, n.value);
					n = n.next;
				}
			}
		}
	}

	public void printAll() {
		for (MyHashMapSeparateChain<K, V>.HashNode<K, V> key : bucket) {
			if (key != null) {
				System.out.println(key.value);
			}
		}
	}

	public class HashNode<K, V> {
		K key;
		V value;
		HashNode<K, V> next;

		public HashNode() {
			this.key = key;
			this.value = value;
		}

	}
}
