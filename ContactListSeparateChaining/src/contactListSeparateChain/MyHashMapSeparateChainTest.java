package contactListSeparateChain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MyHashMapSeparateChainTest {

	private MyHashMapSeparateChain<String, String> testMhms;

	@Before
	public void setUp() throws Exception {
		testMhms = new MyHashMapSeparateChain<>();
		testMhms.add("first", "dog"); // 2
		testMhms.add("second", "cat"); // 0
	}

	@Test
	public void testGet() {
		String testString = testMhms.get("first");
		assertEquals(testString, "dog");
		assertNull(testMhms.get("something"));
	}

	@Test
	public void testRemove() {
		testMhms.add("third", "okapi"); // hashCode 9
		testMhms.add("fourth", "beaver"); // hashCode 8
		testMhms.add("fifth", "horse"); // hashCode 9
		String removed = testMhms.remove("third");
		assertEquals(removed, "okapi");
		removed = testMhms.remove("first");
		assertEquals(removed, "dog");
		assertNull(testMhms.remove("first"));
		assertNull(testMhms.remove("third"));

	}

	@Test
	public void testAdd() {
		testMhms.add("third", "okapi"); // hashCode 9
		String added = testMhms.get("third");
		assertEquals(added, "okapi");
		testMhms.add("fourth", "beaver"); // hashCode 8
		testMhms.add("fifth", "horse"); // hashCode 9
		added = testMhms.get("fifth");
		assertEquals(added, "horse");
		testMhms.add("sixth", "giraffe"); // hashCode 0
		testMhms.add("seventh", "zebra"); // 1
		testMhms.add("eighth", "bunny"); // 7,goes over the threshold(rehashes)
		testMhms.add("nineth", "unicorn");
		added = testMhms.get("nineth");
		assertEquals(added, "unicorn");
	}

}
