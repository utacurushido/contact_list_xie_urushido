package contactList;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ContactTest {
	
	private Contact contactTest;

	@Before
	public void setUp() throws Exception {
		contactTest = new Contact("User 1", "1234567891");
	}

	@Test
	public void testGetName() {
		String name = contactTest.getName();
		assertEquals("User 1", name);
	}

	@Test
	public void testGetNumber() {
		String number = contactTest.getNumber();
		assertEquals("1234567891", number);
	}

	@Test
	public void testToString() {
		String contact = contactTest.toString();
		assertEquals("User 1 1234567891", contact);
	}

}
