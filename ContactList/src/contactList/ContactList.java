package contactList;

import java.util.HashMap;
import java.util.Scanner;

public class ContactList {

	HashMap<String, Contact> contactListName;
	HashMap<String, Contact> contactListNumber;

	public ContactList() {
		contactListName = new HashMap<String, Contact>();
		contactListNumber = new HashMap<String, Contact>();
	}

	//The average running time is theta(1) because HashMap has load factor of 0.75 
	//and the linked list attached to the bucket almost never gets n elements long. 
	//hashing and adding take constant time.
	public boolean insert(String name, String number) {
		number = removeCharacters(number);
		if (!containsName(name) && !containsNumber(number)) {
			if (isValid(number)) {
				Contact newContact = new Contact (name, number);
				contactListName.put(name, newContact);
				contactListNumber.put(number, newContact);
				return true;
			}
		}
		return false;
	}

	//The average running time is theta(1) because removing takes constant time and for the same reason as above.
	public boolean deleteByNumber(String number) {
		if (!containsNumber(number)) {
			return false;
		} else  {
			contactListName.remove(contactListNumber.get(number).getName());
			contactListNumber.remove(number);
			return true;
		}
	}

	//The average running time is theta(1) for the same reason as above 
	public boolean deleteByName(String name) {
		if (!containsName(name)) {
			return false;
		} else  {
			contactListNumber.remove(contactListName.get(name).getNumber());
			contactListName.remove(name);
			return true;
		}
	}

	//The average running time is theta(1) because get takes average theta(1) time to operate for the same reason as above
	public boolean containsName(String name) {
		return contactListName.get(name) != null;
	}

	//The average running time is theta(1) for the same reason as above
	public boolean containsNumber(String number) {
		number = removeCharacters(number);
		return contactListNumber.get(number) != null;

	}

	//The average running time is theta(1) because contains and get both take average theta(1)
	public Contact findByName(String name) {
		if (!containsName(name)) {
			return null;
		} else {
			return contactListName.get(name);
		}
	}

	//The average running time is theta(1) for the same reason as above
	public Contact findByNumber(String number) {
		number = removeCharacters(number);
		if (!containsNumber(number)) {
			return null;
		} else {
			return contactListNumber.get(number);
		}
	}

	//The running time is theta(1) because it only returns the a value of variable 
	public int size(){
		return contactListNumber.size();
	}
	
	//The running time is theta(n) because it traverses and print every single one of n elements stored on hashMap
	public void printAllContacts() {
		for (String key : contactListName.keySet()) {
			System.out.println(contactListName.get(key).toString());
		}
	}
	//The running time is theta(1) it takes constant operations to replace special cases and assign it
	public String removeCharacters(String number) {
		number = number.replaceAll("[^\\d]", "");
		return number;
	}

	//The running time is theta(1) it comparing and returning take constant time. 
	public boolean isValid(String number) {
		return number.length() == 10;
	}

	public static void main(String[] args) {
		ContactList pb = new ContactList();
		pb.insert("Tom", "1234567890");
		pb.insert("Tim Lee", "(123)456-6789");
		
		System.out.println("printing all contacts");
		pb.printAllContacts();
		System.out.println();
		
		System.out.println("Searching for Tim Lee");
		System.out.println(pb.findByName("Tim Lee").toString());
		System.out.println();
		
		System.out.println("Deleting Tim Lee");
		pb.deleteByName("Tim Lee");
		System.out.println("The list is now: ");
		pb.printAllContacts();
		System.out.println();

		System.out.println("Searching for 1234567890");
		System.out.println(pb.findByNumber("1234567890"));
		System.out.println();

		System.out.println("Putting Tim Lee back on");
		pb.insert("Tim Lee", "1234566789");
		System.out.println("printing all contacts");
		pb.printAllContacts();
		System.out.println("The number of Contacts in this contact list is");
		System.out.println(pb.size());
		System.out.println();
		
		System.out.println("Searching for 1234566789");
		System.out.println(pb.findByNumber("1234566789"));
		
		System.out.println("Searching for Tim Lee");
		System.out.println(pb.findByName("Tim Lee"));

		pb.printAllContacts();

		//		boolean exit = false;
		//		while(!exit) {
		//			System.out.println("\nPlease enter the prompt number of what you would like to do: ");
		//			System.out.println("1:Insert a contact to this contact list \n2:Delte a contact by name \n3:Delete a contact by number\n4:Look up a contact by name\n5:Look up a contact by number\n6:Print all the contacts on this contact list\n7:Exist" );
		//			String promptNumber;
		//			promptNumber = pb.input.next();
		//			switch (promptNumber) {
		//			case "1":
		//				pb.input.nextLine();
		//				System.out.println("What is the contact name?");
		//				String name =pb.input.nextLine();
		//				System.out.println("What is the contact number?");
		//				String number = pb.input.nextLine();
		//				if(pb.insert(name, number)) {
		//					System.out.println("Contact added");
		//				}else {
		//					System.out.println("Failed to add. We can't accept the name or the number that is already on the list.");
		//				}
		//				break;
		//			case "2":
		//				pb.input.nextLine();
		//				System.out.println("What is the name of the contact you would like to delete?");
		//				name = pb.input.nextLine();
		//				if(pb.deleteByName(name)) {
		//					System.out.println("Contact deleted");
		//				}else {
		//					System.out.println("This name was not found. Could not delete.");
		//				}
		//				break;
		//			case "3":
		//				pb.input.nextLine();
		//				System.out.println("What is the number of the contact you would like to delete?");
		//				number = pb.input.nextLine();
		//				if(pb.deleteByName(number)) {
		//					System.out.println("Contact deleted");
		//				}else {
		//					System.out.println("This number was not found. Could not delete.");
		//				}
		//				break;
		//			case "4":
		//				pb.input.nextLine();
		//				System.out.println("What is the name of the contact you would like to find?");
		//				String nameToFind = pb.input.nextLine();
		//				if(pb.findByName(nameToFind)!=null){
		//					System.out.println(pb.findByName(nameToFind).toString());
		//				}else {
		//					System.out.println("No contact with that name was found");
		//				}
		//				break;
		//			case "5":
		//				pb.input.nextLine();
		//				System.out.println("What is the number of the contact you would like to find?");
		//				String numberToFind = pb.input.nextLine();
		//				if(pb.findByNumber(numberToFind)!=null){
		//					System.out.println(pb.findByNumber(numberToFind).toString());
		//				}else {
		//					System.out.println("No contact with that number was found");
		//				}
		//				break;
		//			case "6":
		//				pb.printAllContacts();
		//				break;
		//			case "7":
		//				exit = true;
		//				break;
		//			default:
		//				System.out.println("Invalid input. Please put 1 through 6 ");
		//				break;
		//			}



	}


}
