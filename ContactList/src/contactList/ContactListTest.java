package contactList;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ContactListTest {

	private ContactList contactListTest; 
	
	@Before
	public void setUp() throws Exception {
		contactListTest = new ContactList();
		contactListTest.insert("User 1", "123-456-7890");
		contactListTest.insert("User 2", "123-456-7891");
	}

	@Test
	public void testFindByName () {
		Contact contactTest = contactListTest.findByName("User 1");
		assertNotNull(contactTest);
		assertEquals("User 1", contactTest.getName());
		assertEquals("1234567890", contactTest.getNumber());
		assertNull(contactListTest.findByName("User X"));
	}

	@Test
	public void testFindByNumber () {
		Contact contactTest = contactListTest.findByNumber("123-456-7890");
		assertNotNull(contactTest);
		assertEquals("User 1", contactTest.getName());
		assertEquals("1234567890", contactTest.getNumber());
		assertNull(contactListTest.findByNumber("123456799"));
	}

	@Test
	public void testInsert () {
		boolean insertOrNot = contactListTest.insert("User 1", "123-456-7890");
		assertFalse(insertOrNot);
		insertOrNot = contactListTest.insert("User 1", "123-456-7899");
		assertFalse(insertOrNot);
		insertOrNot = contactListTest.insert("User 3", "123-456-7890");
		assertFalse(insertOrNot);
		insertOrNot = contactListTest.insert("User 3", "123-456-7899");
		assertTrue(insertOrNot);
	}
	@Test
	public void testDeleteByName () {
		boolean deleteOrNot = contactListTest.deleteByName("User 3");
		assertFalse(deleteOrNot);
		deleteOrNot = contactListTest.deleteByName("User 1");
		assertTrue(deleteOrNot);
		Contact contactTest = contactListTest.findByName("User 1");
		assertNull(contactTest);
		contactTest = contactListTest.findByNumber("123-456-7890");
		assertNull(contactTest);
	}
	@Test
	public void testDeleteByNumber () {
		boolean deleteOrNot = contactListTest.deleteByNumber("1234567892");
		assertFalse(deleteOrNot);
		deleteOrNot = contactListTest.deleteByNumber("1234567890");
		assertTrue(deleteOrNot);
		Contact contactTest = contactListTest.findByName("User 1");
		assertNull(contactTest);
		contactTest = contactListTest.findByNumber("123-456-7890");
		assertNull(contactTest);
	}
	
	@Test
	public void testContainsNumber () {
		boolean containsOrNot = contactListTest.containsNumber("1234567892");
		assertFalse(containsOrNot);
		containsOrNot = contactListTest.containsNumber("1234567890");
		assertTrue(containsOrNot);
		containsOrNot = contactListTest.containsNumber("123-456-7890");
		assertTrue(containsOrNot);
	}
	
	@Test
	public void testContainsName () {
		boolean containsOrNot = contactListTest.containsName("User X");
		assertFalse(containsOrNot);
		containsOrNot = contactListTest.containsName("User 1");
		assertTrue(containsOrNot);
	}
}
